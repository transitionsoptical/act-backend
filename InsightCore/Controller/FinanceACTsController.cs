﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.ComponentModel;

namespace InsightCore.Controller
{

    [ApiController]
    public class FinanceACTsController : ControllerBase
    {
        private readonly ACTContext _context;
        private readonly EmployeeMasterContext _contextEmployee;

        public FinanceACTsController(ACTContext context, EmployeeMasterContext contextEmployee)
        {
            _context = context;
            _contextEmployee = contextEmployee;
        }

        // GET: api/FinanceACTs
        [Route("api/[controller]")]
        [HttpGet]
        public IEnumerable<FinanceACT> GetFinanceACTs()
        {
            return _context.FinanceACTs.OrderByDescending(x => x.Id);
        }

        // GET: api/FinanceACTs/5
        [Route("api/[controller]/{id}")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFinanceACT([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeACT = await _context.FinanceACTs.FindAsync(id);

            if (financeACT == null)
            {
                return NotFound();
            }

            return Ok(financeACT);
        }

        // PUT: api/FinanceACTs/5
        [Route("api/UpdateFinanceAct/{id}")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFinanceACT([FromRoute] int id, [FromBody] FinanceACT financeACT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != financeACT.Id)
            {
                return BadRequest();
            }

            _context.Entry(financeACT).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FinanceACTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FinanceACTs
        [Route("api/[controller]")]
        [HttpPost]
        public async Task<IActionResult> PostFinanceACT([FromBody] FinanceACT financeACT)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            var configuration = builder.Build();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.FinanceACTs.Add(financeACT);
            await _context.SaveChangesAsync();

            string emailAddress = "", emailAddress2 = "", sMessage = "", sMessage2 = "", sSubject = "", sSubject2 = "", email = "";
            MailMessage oMessage = new MailMessage();
            MailMessage oMessage2 = new MailMessage();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            System.Text.StringBuilder sb2 = new System.Text.StringBuilder();

            emailAddress = financeACT.CreatedByEmail;

            if (emailAddress != null)
                oMessage.To.Add(new MailAddress(emailAddress));

            oMessage.CC.Add(new MailAddress("actapprovals@transitions.com"));

            oMessage.Bcc.Add(new MailAddress(("coren.silva@essilor.com")));
            oMessage.Bcc.Add(new MailAddress(("coren.silva@transitions.com")));

            sb.AppendFormat("<p>You have submitted an ACT Workflow <a href='{0}'>{1}</a></p>", configuration["ACTViewLink"].ToString() + financeACT.Id, financeACT.ProjectName);
            sb.Append("<p>Approval process has now started.</ p>");

            sMessage = sb.ToString();

            SendMail(sMessage, "Authorization for Capital Transaction - Submitted", oMessage);



            if (financeACT.WFCurrentApprover == "EHSApprover")
            {
                emailAddress2 = financeACT.WFEHSApprover;

            }
            else if (financeACT.WFCurrentApprover == "EHSReviewer")
            {
                emailAddress2 = financeACT.WFEHSReviewer;
            }
            else if (financeACT.WFCurrentApprover == "S&PReviewer")
            {
                emailAddress2 = financeACT.WFSPReviewer;
            }
            else if (financeACT.WFCurrentApprover == "BU1Approver")
            {
                emailAddress2 = financeACT.WFBU1Approver;
            }
            else if (financeACT.WFCurrentApprover == "BU2Approver")
            {
                emailAddress2 = financeACT.WFBU2Approver;
            }
            else if (financeACT.WFCurrentApprover == "FinanceApprover1")
            {
                emailAddress2 = financeACT.WFFinance1Approval;
            }
            else if (financeACT.WFCurrentApprover == "GlobalFinanceApprover1")
            {
                emailAddress2 = financeACT.WFGlobalFinanceApprover1;
            }
            else if (financeACT.WFCurrentApprover == "FinalApprover1")
            {
                emailAddress2 = financeACT.WFFinalApprover1;
            }
            else if (financeACT.WFCurrentApprover == "FinalApprover2")
            {
                emailAddress2 = financeACT.WFFinalApprover2;
            }
            else if (financeACT.WFCurrentApprover == "FinalApprover3")
            {
                emailAddress2 = financeACT.WFFinalApprover3;
            }
            else if (financeACT.WFCurrentApprover == "FinalApprover4")
            {
                emailAddress2 = financeACT.WFFinalApprover4;
            }
            else if (financeACT.WFCurrentApprover == "FinalApprover5")
            {
                emailAddress2 = financeACT.WFFinalApprover5;
            }
            else if (financeACT.WFCurrentApprover == "FinalApprover6")
            {
                emailAddress2 = financeACT.WFFinalApprover6;
            }


            email = getEmail(emailAddress2);

            if (email != null)
                oMessage2.To.Add(new MailAddress(email));

            oMessage.CC.Add(new MailAddress("actapprovals@transitions.com"));
            oMessage.CC.Add(new MailAddress((financeACT.CreatedByEmail)));
            oMessage2.Bcc.Add(new MailAddress(("coren.silva@essilor.com")));
            oMessage2.Bcc.Add(new MailAddress(("coren.silva@transitions.com")));

            if (financeACT.DoesNeedEHSReview == true)
            {
                sSubject2 = "Authorization for Capital Transaction - ACT EH&S Approval Task";
            }
            else
            {
                sSubject2 = "Authorization for Capital Transaction - ACT Approval Task";
            }

            sb2.AppendFormat("<p><strong>ACT Approval Task assigned by {0} on {1} </strong></p>", financeACT.CreatedBy, financeACT.Created);
            sb2.Append("<p>To complete this task:</ p>");
            sb2.AppendFormat("<p>1. Review <a href='{0}'>{1}</a>.</ p>", configuration["ACTViewLink"].ToString() + financeACT.Id, financeACT.ProjectName);
            sb2.Append("<p>2.Check EH&S requirements are met/not met and complete the workflow.</ p>");
            sb2.Append("<table>");
            sb2.Append("<tbody>");
            sb2.Append("<tr><td><strong>Details</strong></td>");
            sb2.Append("<td></td></tr>");
            sb2.Append("<tr><td><strong>Project Name</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectName);
            sb2.Append("<tr><td><strong>Project Manager</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectManagerName);
            sb2.Append("<tr><td><strong>Location</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.Location);
            //sb2.Append("<tr><td><strong>Entity</strong></td>");
            //sb2.AppendFormat("<td>{0}</td></tr>", financeACT.Entity);
            sb2.Append("<tr><td><strong>Classification</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.Classification);
            sb2.Append("<tr><td><strong>Project Description</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectDescription);
            sb2.Append("<tr><td><strong>Start Construction</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.StartContruction);
            sb2.Append("<tr><td><strong>Start-up Date</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.StartUpDate);
            sb2.Append("<tr><td><strong>Economic Life in Years</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.EconomicLife);
            sb2.Append("<tr><td><strong>Project IRR % (if applicable</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectIRR);
            sb2.Append("<tr><td><strong>Project Cash Payback Years (if applicable)</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectCashPaybackYears);
            sb2.Append("<tr><td><strong>Project ROC % (if applicable)</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectROC);
            sb2.Append("<tr><td><strong>ACT Amount in $ USD</strong></td>");
            sb2.AppendFormat("<td>${0}</td></tr>", financeACT.ACTAmount);
            sb2.Append("<tr><td><strong>ACT # (if known)</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.ACTNumber);
            sb2.Append("<tr><td><strong>ACT# recorded to MFGPRO</strong></td>");
            sb2.AppendFormat("<td>{0}</td></tr>", financeACT.ActRecordedtoMFGPRO);
            sb2.Append("</tbody>");
            sb2.Append("</table>");

            sMessage2 = sb2.ToString();

            SendMail(sMessage2, sSubject2, oMessage2);



            return CreatedAtAction("GetFinanceACT", new { id = financeACT.Id }, financeACT);
        }

        // DELETE: api/FinanceACTs/5
        [Route("api/deleteFinanceACTs/{id}")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFinanceACT([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeACT = await _context.FinanceACTs.FindAsync(id);
            if (financeACT == null)
            {
                return NotFound();
            }

            _context.FinanceACTs.Remove(financeACT);
            await _context.SaveChangesAsync();

            return Ok(financeACT);
        }
        

        [Route("api/SendNotification")]
        [HttpPost()]
        public void sendNotification([FromBody] FinanceACT financeACT)
        {
            var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json");

            var configuration = builder.Build();

            var buresult = _context.FinanceACTBusinessUnits.SingleOrDefault(b => b.Title == financeACT.Location);

            if (financeACT.Status == "Approved")
            {
                string emailAddress3 = "", sMessage3 = "";
                MailMessage oMessage = new MailMessage();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                emailAddress3 = buresult.FinanceEmail;
                //emailAddress3 = "jbailey@transitions.com;pdooley@transitions.com";

                string[] faApprover = emailAddress3.Split(";");
                for (int i = 0; i < faApprover.Length; i++)
                {
                    oMessage.To.Add(new MailAddress((faApprover[i].ToString().Trim())));
                }

                oMessage.CC.Add(new MailAddress("actapprovals@transitions.com"));
                oMessage.CC.Add(new MailAddress(financeACT.CreatedByEmail));
                oMessage.Bcc.Add(new MailAddress(("coren.silva@essilor.com")));
                oMessage.Bcc.Add(new MailAddress(("coren.silva@transitions.com")));

                //if (emailAddress3 != null)
                //    oMessage.To.Add(new MailAddress(emailAddress3));

                sb.Append("<p>Capital authorization request has been approved.</ p>");
                sb.AppendFormat("<p>Kindly check the link below to view the request and assign a unique ACT#. <a href='{0}'>{1}</a><p>", configuration["ACTEditLink"].ToString() + financeACT.Id, financeACT.ProjectName);

                sMessage3 = sb.ToString();

                SendMail(sMessage3, "Authorization for Capital Transaction - ACT Approved assign ACT #", oMessage);
            }
            else if (financeACT.Status == "Pending") {
                string emailAddress = "", emailAddress2 = "", sMessage = "", sMessage2 = "", sSubject = "", sSubject2 = "", email = "";
                MailMessage oMessage = new MailMessage();

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                oMessage.To.Add(new MailAddress("dmcgee@transitions.com"));
                oMessage.CC.Add(new MailAddress("actapprovals@transitions.com"));
                oMessage.CC.Add(new MailAddress("pdooley@transitions.com"));
                oMessage.Bcc.Add(new MailAddress("coren.silva@essilor.com"));

                //if (financeACT.DoesNeedEHSReview == true)
                //{
                //    sSubject = "Authorization for Capital Transaction - ACT EH&S Approval Task";
                //}
                //else
                //{
                //    sSubject = "Authorization for Capital Transaction - ACT Approval Task";
                //}

                sb.Append("<p><strong>ACT Approval Task assigned by Peter Dooley on 10/14/2020 12:00:00 AM </strong></p>");
                sb.Append("<p>To complete this task:</ p>");
                sb.Append("<p>1. Review <a href='https://insight-apps.transitions.com/act/displayForm/686'>Air Compressor Upgrades-Analytical Lab-Building 2</a>.</ p>");
                sb.Append("<p>2.Check EH&S requirements are met/not met and complete the workflow.</ p>");
                sb.Append("<table>");
                sb.Append("<tbody>");
                sb.Append("<tr><td><strong>Details</strong></td>");
                sb.Append("<td></td></tr>");
                sb.Append("<tr><td><strong>Project Name</strong></td>");
                sb.Append("<td>Air Compressor Upgrades-Analytical Lab-Building 2</td></tr>");
                sb.Append("<tr><td><strong>Project Manager</strong></td>");
                sb.Append("<td>Farrelly, Tim</td></tr>");
                sb.Append("<tr><td><strong>Location</strong></td>");
                sb.Append("<td>RandD</td></tr>");
                //sb2.Append("<tr><td><strong>Entity</strong></td>");
                //sb2.AppendFormat("<td>{0}</td></tr>", financeACT.Entity);
                sb.Append("<tr><td><strong>Classification</strong></td>");
                sb.Append("<td>R&D</td></tr>");
                sb.Append("<tr><td><strong>Project Description</strong></td>");
                sb.Append("<td>TOL is requesting funding for an additional air compressor unit to support the Analytical lab infrastructure to Building 2. The recent addition of optical bench test equipment within the lab has required the use of additional air supply to support lab equipment. This now requires the use of the current 2nd backup air compressor to be used full time to support the new air supply requirements. The addition of a new 3rd Air compressor will bring back key equipment redundancy to the air compresors configuration for the lab. This will ensure a relaible source of air supply at all times to the lab. The new configuration will allow 2 air compressors to be run full time, with the 3rd as back up. Control programming will allow a weekly compressor rotation of 2 running and the 3rd on standby if issues occur to the other 2 units.</td></tr>");
                sb.Append("<tr><td><strong>Start Construction</strong></td>");
                sb.Append("<td>10/31/2020 12:00:00 AM</td></tr>");
                sb.Append("<tr><td><strong>Start-up Date</strong></td>");
                sb.Append("<td>10/31/2020 12:00:00 AM</td></tr>");
                sb.Append("<tr><td><strong>Economic Life in Years</strong></td>");
                sb.Append("<td>10</td></tr>");
                sb.Append("<tr><td><strong>Project IRR % (if applicable</strong></td>");
                sb.Append("<td>0%</td></tr>");
                sb.Append("<tr><td><strong>Project Cash Payback Years (if applicable)</strong></td>");
                sb.Append("<td></td></tr>");
                sb.Append("<tr><td><strong>Project ROC % (if applicable)</strong></td>");
                sb.Append("<td>0%</td></tr>");
                sb.Append("<tr><td><strong>ACT Amount in $ USD</strong></td>");
                sb.AppendFormat("<td>$11778</td></tr>");
                sb.Append("<tr><td><strong>ACT # (if known)</strong></td>");
                sb.AppendFormat("<td>0132</td></tr>");
                sb.Append("<tr><td><strong>ACT# recorded to MFGPRO</strong></td>");
                sb.AppendFormat("<td>False</td></tr>");
                sb.Append("</tbody>");
                sb.Append("</table>");

                sMessage = sb.ToString();

                SendMail(sMessage, "Authorization for Capital Transaction - ACT EH&S Approval Task", oMessage);
            }
        }

        // GET: api/getFilteredAct
        [Route("api/getFilteredAct")]
        [HttpGet("{currentApprover}")]
        public IActionResult GetFilteredAct([FromQuery] String currentApprover)
        {

            var query = _context.FinanceACTs.FromSql("Select * from FinanceACTs where WFCurrentApproverName={0} and Status = 'Pending'", currentApprover)
                .ToList();


            return Ok(query);
        }

        private bool FinanceACTExists(int id)
        {
            return _context.FinanceACTs.Any(e => e.Id == id);
        }

        private void SendMail(String strMessage, String sSubject, MailMessage oMessageApprovers)
        {
            oMessageApprovers.From = new MailAddress("noreply@transitions.com", "noreply-");
            oMessageApprovers.Subject = sSubject;
            oMessageApprovers.Body = strMessage;
            oMessageApprovers.IsBodyHtml = true;
            SmtpClient oSMTPClient = new SmtpClient("smtp.gmail.com", 587);
            try
            {
                oSMTPClient.UseDefaultCredentials = false;
                oSMTPClient.EnableSsl = true;
                oSMTPClient.Credentials = new NetworkCredential("external.relay@transitions.com", "ydtrrputnrcimehf");
                // oSMTPClient.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);

                object userState = oMessageApprovers;
                oSMTPClient.SendCompleted += new SendCompletedEventHandler(SmtpClient_OnCompleted);

                oSMTPClient.SendAsync(oMessageApprovers, userState);

            }
            catch (Exception ex)
            {
            }

        }
        public static void SmtpClient_OnCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //Get the Original MailMessage object
            MailMessage mail = (MailMessage)e.UserState;

            //write out the subject
            string subject = mail.Subject;

            if (e.Cancelled)
            {
                Console.WriteLine("Send canceled for mail with subject [{0}].", subject);
            }
            if (e.Error != null)
            {
                Console.WriteLine("Error {1} occurred when sending mail [{0}] ", subject, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message [{0}] sent.", subject);
            }
        }

        string getEmail(string name)
        {
            var emailAddress = "";
            emailAddress = _contextEmployee.EmployeeMasters.Where(x => x.Name == name)
                     .Select(a => a.Email).FirstOrDefault();
            return emailAddress;
        }
    }
}