﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;

namespace InsightCore.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class FinanceACTClassificationsController : ControllerBase
    {
        private readonly ACTContext _context;

        public FinanceACTClassificationsController(ACTContext context)
        {
            _context = context;
        }

        // GET: api/FinanceACTClassifications
        [HttpGet]
        public IEnumerable<FinanceACTClassification> GetFinanceACTClassifications()
        {
            return _context.FinanceACTClassifications;
        }

        // GET: api/FinanceACTClassifications/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFinanceACTClassification([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeACTClassification = await _context.FinanceACTClassifications.FindAsync(id);

            if (financeACTClassification == null)
            {
                return NotFound();
            }

            return Ok(financeACTClassification);
        }

        // PUT: api/FinanceACTClassifications/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFinanceACTClassification([FromRoute] int id, [FromBody] FinanceACTClassification financeACTClassification)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != financeACTClassification.Id)
            {
                return BadRequest();
            }

            _context.Entry(financeACTClassification).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FinanceACTClassificationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FinanceACTClassifications
        [HttpPost]
        public async Task<IActionResult> PostFinanceACTClassification([FromBody] FinanceACTClassification financeACTClassification)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.FinanceACTClassifications.Add(financeACTClassification);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFinanceACTClassification", new { id = financeACTClassification.Id }, financeACTClassification);
        }

        // DELETE: api/FinanceACTClassifications/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFinanceACTClassification([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeACTClassification = await _context.FinanceACTClassifications.FindAsync(id);
            if (financeACTClassification == null)
            {
                return NotFound();
            }

            _context.FinanceACTClassifications.Remove(financeACTClassification);
            await _context.SaveChangesAsync();

            return Ok(financeACTClassification);
        }

        private bool FinanceACTClassificationExists(int id)
        {
            return _context.FinanceACTClassifications.Any(e => e.Id == id);
        }
    }
}