﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;

namespace InsightCore.Controller
{
    
    [ApiController]
    public class FinanceActLocationsController : ControllerBase
    {
        private readonly ACTContext _context;

        public FinanceActLocationsController(ACTContext context)
        {
            _context = context;
        }

        // GET: api/FinanceActLocations
        [Route("api/[controller]")]
        [HttpGet]
        public IEnumerable<FinanceActLocation> GetFinanceActLocation()
        {
            return _context.FinanceActLocation;
        }

        // GET: api/FinanceActLocations/5
        [Route("api/[controller]")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFinanceActLocation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeActLocation = await _context.FinanceActLocation.FindAsync(id);

            if (financeActLocation == null)
            {
                return NotFound();
            }

            return Ok(financeActLocation);
        }

        // PUT: api/FinanceActLocations/5
        [Route("api/[controller]")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFinanceActLocation([FromRoute] int id, [FromBody] FinanceActLocation financeActLocation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != financeActLocation.id)
            {
                return BadRequest();
            }

            _context.Entry(financeActLocation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FinanceActLocationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FinanceActLocations
        [Route("api/[controller]")]
        [HttpPost]
        public async Task<IActionResult> PostFinanceActLocation([FromBody] FinanceActLocation financeActLocation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.FinanceActLocation.Add(financeActLocation);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFinanceActLocation", new { id = financeActLocation.id }, financeActLocation);
        }

        // DELETE: api/FinanceActLocations/5
        [Route("api/[controller]")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFinanceActLocation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeActLocation = await _context.FinanceActLocation.FindAsync(id);
            if (financeActLocation == null)
            {
                return NotFound();
            }

            _context.FinanceActLocation.Remove(financeActLocation);
            await _context.SaveChangesAsync();

            return Ok(financeActLocation);
        }

        

        private bool FinanceActLocationExists(int id)
        {
            return _context.FinanceActLocation.Any(e => e.id == id);
        }
    }
}