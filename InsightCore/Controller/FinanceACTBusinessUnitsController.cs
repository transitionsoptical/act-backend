﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;

namespace InsightCore.Controller
{
    
    [ApiController]
    public class FinanceACTBusinessUnitsController : ControllerBase
    {
        private readonly ACTContext _context;

        public FinanceACTBusinessUnitsController(ACTContext context)
        {
            _context = context;
        }

        // GET: api/FinanceACTBusinessUnits
        [Route("api/[controller]")]
        [HttpGet]
        public IEnumerable<FinanceACTBusinessUnit> GetFinanceACTBusinessUnits()
        {
            return _context.FinanceACTBusinessUnits;
        }

        // GET: api/FinanceACTBusinessUnits/5
        [Route("api/[controller]")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFinanceACTBusinessUnit([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeACTBusinessUnit = await _context.FinanceACTBusinessUnits.FindAsync(id);

            if (financeACTBusinessUnit == null)
            {
                return NotFound();
            }

            return Ok(financeACTBusinessUnit);
        }

        // PUT: api/FinanceACTBusinessUnits/5
        [Route("api/[controller]")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFinanceACTBusinessUnit([FromRoute] int id, [FromBody] FinanceACTBusinessUnit financeACTBusinessUnit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != financeACTBusinessUnit.Id)
            {
                return BadRequest();
            }

            _context.Entry(financeACTBusinessUnit).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FinanceACTBusinessUnitExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        [Route("api/getApprovers/")]
        [HttpGet("{title}")]
        public IEnumerable<FinanceACTBusinessUnit> GetApproverFilter([FromQuery] string title)
        {
            var query = _context.FinanceACTBusinessUnits
              .AsQueryable()
              .Where(x =>
                (x.Title == title)
              )
              .ToList();

            return query;
        }
        // POST: api/FinanceACTBusinessUnits
        [Route("api/[controller]")]
        [HttpPost]
        public async Task<IActionResult> PostFinanceACTBusinessUnit([FromBody] FinanceACTBusinessUnit financeACTBusinessUnit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.FinanceACTBusinessUnits.Add(financeACTBusinessUnit);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFinanceACTBusinessUnit", new { id = financeACTBusinessUnit.Id }, financeACTBusinessUnit);
        }

        // DELETE: api/FinanceACTBusinessUnits/5
        [Route("api/[controller]")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFinanceACTBusinessUnit([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeACTBusinessUnit = await _context.FinanceACTBusinessUnits.FindAsync(id);
            if (financeACTBusinessUnit == null)
            {
                return NotFound();
            }

            _context.FinanceACTBusinessUnits.Remove(financeACTBusinessUnit);
            await _context.SaveChangesAsync();

            return Ok(financeACTBusinessUnit);
        }

        private bool FinanceACTBusinessUnitExists(int id)
        {
            return _context.FinanceACTBusinessUnits.Any(e => e.Id == id);
        }
    }
}