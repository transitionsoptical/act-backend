﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using InsightCore.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace InsightCore.Controller
{
    
    [ApiController]
    public class ACTApprovalController : ControllerBase
    {
        private readonly ACTContext _context;
        private readonly EmployeeMasterContext _contextEmployee;
        public ACTApprovalController(ACTContext context, EmployeeMasterContext contextEmployee)
        {
            _context = context;
            _contextEmployee = contextEmployee;
        }
        [Route("api/SetApprovalStatus")]
        [HttpPost()]
        public void setApprovalStatus([FromBody] FinanceACT financeACT)
        {
            var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json");

            var configuration = builder.Build();

            var result = _context.FinanceACTs.SingleOrDefault(b => b.Id == financeACT.Id);

            var buresult = _context.FinanceACTBusinessUnits.SingleOrDefault(b => b.Title == financeACT.Location);



            if (result != null)
            {
                result.Status = financeACT.Status;
                result.WFEHSApproverApproval = financeACT.WFEHSApproverApproval;
                result.WFEHSApproverComment = financeACT.WFEHSApproverComment;
                result.WFEHSReviewerApproval = financeACT.WFEHSReviewerApproval;
                result.WFEHSReviewerComment = financeACT.WFEHSReviewerComment;
                result.WFSPReviewerApproval = financeACT.WFSPReviewerApproval;
                result.WFSPReviewerComment = financeACT.WFSPReviewerComment;
                result.WFBU1ApproverApproval = financeACT.WFBU1ApproverApproval;
                result.WFBU1ApproverComment = financeACT.WFBU1ApproverComment;
                result.WFBU2ApproverApproval = financeACT.WFBU2ApproverApproval;
                result.WFBU2ApproverComment = financeACT.WFBU2ApproverComment;
                result.WFFinanceApprover1Approval = financeACT.WFFinanceApprover1Approval;
                result.WFFinanceApprover1Comment = financeACT.WFFinanceApprover1Comment;
                result.WFGlobalFinanceApprover1Approval = financeACT.WFGlobalFinanceApprover1Approval;
                result.WFGlobalFinanceApprover1Comment = financeACT.WFGlobalFinanceApprover1Comment;
                result.WFFinalApprover1Approval = financeACT.WFFinalApprover1Approval;
                result.WFFinalApprover1Comment = financeACT.WFFinalApprover1Comment;
                result.WFFinalApprover2Approval = financeACT.WFFinalApprover2Approval;
                result.WFFinalApprover2Comment = financeACT.WFFinalApprover2Comment;
                result.WFFinalApprover3Approval = financeACT.WFFinalApprover3Approval;
                result.WFFinalApprover3Comment = financeACT.WFFinalApprover3Comment;
                result.WFFinalApprover4Approval = financeACT.WFFinalApprover4Approval;
                result.WFFinalApprover4Comment = financeACT.WFFinalApprover4Comment;
                result.WFFinalApprover5Approval = financeACT.WFFinalApprover5Approval;
                result.WFFinalApprover5Comment = financeACT.WFFinalApprover5Comment;
                result.WFFinalApprover6Approval = financeACT.WFFinalApprover6Approval;
                result.WFFinalApprover6Comment = financeACT.WFFinalApprover6Comment;
                result.LastApprovedDate = financeACT.LastApprovedDate;


                if (financeACT.Status == "Pending")
                {
                    result.WFCurrentApprover = financeACT.WFCurrentApprover;
                    result.WFCurrentApproverName = financeACT.WFCurrentApproverName;
                    result.ApprovalInProgress = true;
                }
                else if (financeACT.Status == "Reject" || financeACT.Status == "Approved")
                {
                    result.ApprovalInProgress = false;
                    result.WFCurrentApprover = "Done";
                    result.WFCurrentApproverName = "";
                }

                _context.SaveChanges();


                if (financeACT.Status == "Pending")
                {
                    string emailAddress = "", sMessage = "", sSubject = "", email = "", approvedBy = "", sSubjectApproved = "";
                    DateTime date1 = DateTime.ParseExact("16/09/2020", "dd/MM/yyyy", null);
                    DateTime date2 = DateTime.Parse(financeACT.Created.ToString());

                    if (financeACT.WFCurrentApprover == "EHSReviewer")
                    {
                        emailAddress = financeACT.WFEHSReviewer;
                        approvedBy = "EHS APPROVER";
                        //testemail = "ehsrev@gmail.com";
                    }
                    else if (financeACT.WFCurrentApprover == "S&PReviewer")
                    {
                        emailAddress = financeACT.WFSPReviewer;
                        if (financeACT.WFEHSReviewer == "" || financeACT.WFEHSReviewer == null)
                            approvedBy = "EHS APPROVER";
                        else
                            approvedBy = "EHS REVIEWER";
                        //testemail = "sprev@gmail.com";
                    }
                    else if (financeACT.WFCurrentApprover == "BU1Approver")
                    {
                        emailAddress = financeACT.WFBU1Approver;
                        approvedBy = "S&P REVIEWER";
                        //testemail = "bu1@gmail.com";
                    }
                    else if (financeACT.WFCurrentApprover == "BU2Approver")
                    {
                        emailAddress = financeACT.WFBU2Approver;
                        if (date2 > date1)
                            approvedBy = "FINANCE APPROVER";
                        else
                            approvedBy = "BU1 APPROVER";
                        //testemail = "bu2@gmail.com";
                    }
                    else if (financeACT.WFCurrentApprover == "FinanceApprover1")
                    {
                        emailAddress = financeACT.WFFinanceApprover1;
                        if (date2 > date1)
                        {
                            if (financeACT.WFBU1Approver == "" || financeACT.WFBU1Approver == null)
                                approvedBy = "S&P REVIEWER";
                            else
                                approvedBy = "BU1 APPROVER";
                        }
                        else
                            approvedBy = "BU2 APPROVER";
                        //testemail = "finapp@gmail.com";
                    }
                    else if (financeACT.WFCurrentApprover == "GlobalFinanceApprover1")
                    {
                        emailAddress = financeACT.WFGlobalFinanceApprover1;
                        if (date2 > date1)
                            approvedBy = "BU2 APPROVER";
                        else
                            approvedBy = "FINANCE APPROVER";
                        //testemail = "globfinapp@gmail.com";
                    }
                    else if (financeACT.WFCurrentApprover == "FinalApprover1")
                    {
                        emailAddress = financeACT.WFFinalApprover1;
                        approvedBy = "GLOBAL FINANCE APPROVER";
                    }
                    else if (financeACT.WFCurrentApprover == "FinalApprover2")
                    {
                        emailAddress = financeACT.WFFinalApprover2;
                        approvedBy = "FINAL APPROVER 1";
                    }
                    else if (financeACT.WFCurrentApprover == "FinalApprover3")
                    {
                        emailAddress = financeACT.WFFinalApprover3;
                        approvedBy = "FINAL APPROVER 2";
                    }
                    else if (financeACT.WFCurrentApprover == "FinalApprover4")
                    {
                        emailAddress = financeACT.WFFinalApprover4;
                        approvedBy = "FINAL APPROVER 3";
                    }
                    else if (financeACT.WFCurrentApprover == "FinalApprover5")
                    {
                        emailAddress = financeACT.WFFinalApprover5;
                        if (date2 > date1)
                            approvedBy = "GLOBAL FINANCE APPROVER";
                        else
                            approvedBy = "FINAL APPROVER 4";
                        //testemail = "final5@gmail.com";
                    }
                    else if (financeACT.WFCurrentApprover == "FinalApprover6")
                    {
                        emailAddress = financeACT.WFFinalApprover6;
                        approvedBy = "EXEC APPROVER";
                        //testemail = "final6@gmail.com";
                    }

                    //approval notice
                    MailMessage oMessageaapproved = new MailMessage();
                    System.Text.StringBuilder sbapproved = new System.Text.StringBuilder();
                    oMessageaapproved.To.Add(financeACT.CreatedByEmail);
                    oMessageaapproved.Bcc.Add(new MailAddress(("coren.silva@essilor.com")));
                    oMessageaapproved.Bcc.Add(new MailAddress(("coren.silva@transitions.com")));

                    sSubjectApproved = "Authorization for Capital Transaction Approved - " + approvedBy;

                    sbapproved.AppendFormat("<p>This is to notify that your ACT request, with the title: {0}, has been approved by the {1}</p>", financeACT.ProjectName, approvedBy);
                    sbapproved.AppendFormat("<p>To view this request, click on this link: <a href='{0}'>{1}</a>.</ p>", configuration["ACTViewLink"].ToString() + financeACT.Id, configuration["ACTViewLink"].ToString() + financeACT.Id);

                    SendMail(sbapproved.ToString(), sSubjectApproved, oMessageaapproved);


                    //next approval notification
                    MailMessage oMessage = new MailMessage();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    email = getEmail(emailAddress);
                    if (email != null)
                        oMessage.To.Add(new MailAddress(email));


                    oMessage.CC.Add(new MailAddress("actapprovals@transitions.com"));
                    //oMessage.CC.Add(new MailAddress(testemail));
                    oMessage.CC.Add(new MailAddress(financeACT.CreatedByEmail));
                    oMessage.Bcc.Add(new MailAddress(("coren.silva@essilor.com")));
                    oMessage.Bcc.Add(new MailAddress(("coren.silva@transitions.com")));

                    if (financeACT.DoesNeedEHSReview == true)
                    {
                        sSubject = "Authorization for Capital Transaction - ACT EH&S Approval Task";
                    }
                    else
                    {
                        sSubject = "Authorization for Capital Transaction - ACT Approval Task";
                    }


                    sb.AppendFormat("<p><strong>ACT Approval Task assigned by {0} on {1} </strong></p>", financeACT.CreatedBy, financeACT.Created);
                    sb.Append("<p>To complete this task:</ p>");
                    sb.AppendFormat("<p>1. Review <a href='{0}'>{1}</a>.</ p>", configuration["ACTViewLink"].ToString() + financeACT.Id, financeACT.ProjectName);
                    sb.Append("<p>2.Check EH&S requirements are met/not met and complete the workflow.</ p>");
                    sb.Append("<table>");
                    sb.Append("<tbody>");
                    sb.Append("<tr><td><strong>Details</strong></td>");
                    sb.Append("<td></td></tr>");
                    sb.Append("<tr><td><strong>Project Name</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectName);
                    sb.Append("<tr><td><strong>Project Manager</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectManagerName);
                    sb.Append("<tr><td><strong>Location</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.Location);
                    //sb.Append("<tr><td><strong>Entity</strong></td>");
                    //sb.AppendFormat("<td>{0}</td></tr>", financeACT.Entity);
                    sb.Append("<tr><td><strong>Classification</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.Classification);
                    sb.Append("<tr><td><strong>Project Description</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectDescription);
                    sb.Append("<tr><td><strong>Start Construction</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.StartContruction);
                    sb.Append("<tr><td><strong>Start-up Date</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.StartUpDate);
                    sb.Append("<tr><td><strong>Economic Life in Years</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.EconomicLife);
                    sb.Append("<tr><td><strong>Project IRR % (if applicable</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectIRR);
                    sb.Append("<tr><td><strong>Project Cash Payback Years (if applicable)</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectCashPaybackYears);
                    sb.Append("<tr><td><strong>Project ROC % (if applicable)</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.ProjectROC);
                    sb.Append("<tr><td><strong>ACT Amount in $ USD</strong></td>");
                    sb.AppendFormat("<td>${0}</td></tr>", financeACT.ACTAmount);
                    sb.Append("<tr><td><strong>ACT # (if known)</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.ACTNumber);
                    sb.Append("<tr><td><strong>ACT# recorded to MFGPRO</strong></td>");
                    sb.AppendFormat("<td>{0}</td></tr>", financeACT.ActRecordedtoMFGPRO);
                    sb.Append("</tbody>");
                    sb.Append("</table>");


                    sMessage = sb.ToString();

                    SendMail(sMessage, sSubject, oMessage);
                }
                else if (financeACT.Status == "Reject")
                {

                    string emailAddress2 = "", sMessage2 = "", comment = "", testemail = "";
                    MailMessage oMessage = new MailMessage();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();


                    if (financeACT.WFCurrentApprover == "EHSApprover")
                    {
                        comment = financeACT.WFEHSApproverComment;
                        //testemail = "reject-EHSapp@gmail.com";

                    }
                    else if (financeACT.WFCurrentApprover == "EHSReviewer")
                    {
                        comment = financeACT.WFEHSReviewerComment;
                        //testemail = "reject-EHSrev@gmail.com";
                        if (financeACT.WFEHSApprover != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSApprover)));
                        }
                    }
                    else if (financeACT.WFCurrentApprover == "S&PReviewer")
                    {
                        comment = financeACT.WFSPReviewerComment;
                        //testemail = "reject-SPrev@gmail.com";
                        if (financeACT.WFEHSApprover != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSApprover)));
                        }
                        if (financeACT.WFEHSReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSReviewer)));
                        }
                    }
                    else if (financeACT.WFCurrentApprover == "BU1Approver")
                    {
                        comment = financeACT.WFBU1ApproverComment;
                        //testemail = "reject-bu1@gmail.com";
                        if (financeACT.WFEHSApprover != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSApprover)));
                        }
                        if (financeACT.WFEHSReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSReviewer)));
                        }
                        if (financeACT.WFSPReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFSPReviewer)));
                        }
                    }
                    else if (financeACT.WFCurrentApprover == "BU2Approver")
                    {
                        comment = financeACT.WFBU2ApproverComment;
                        //testemail = "reject-bu2@gmail.com";
                        if (financeACT.WFEHSApprover != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSApprover)));
                        }
                        if (financeACT.WFEHSReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSReviewer)));
                        }
                        if (financeACT.WFSPReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFSPReviewer)));
                        }
                        if (financeACT.WFBU1Approver != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFBU1Approver)));
                        }
                        if (financeACT.WFFinanceApprover1 != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFFinanceApprover1)));
                        }
                    }
                    else if (financeACT.WFCurrentApprover == "FinanceApprover1")
                    {
                        comment = financeACT.WFFinanceApprover1Comment;
                        //testemail = "reject-finapp@gmail.com";
                        if (financeACT.WFEHSApprover != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSApprover)));
                        }
                        if (financeACT.WFEHSReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSReviewer)));
                        }
                        if (financeACT.WFSPReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFSPReviewer)));
                        }
                        if (financeACT.WFBU1Approver != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFBU1Approver)));
                        }
                    }
                    else if (financeACT.WFCurrentApprover == "GlobalFinanceApprover1")
                    {
                        comment = financeACT.WFGlobalFinanceApprover1Comment;
                        //testemail = "reject-globfinapp@gmail.com";
                        if (financeACT.WFEHSApprover != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSApprover)));
                        }
                        if (financeACT.WFEHSReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSReviewer)));
                        }
                        if (financeACT.WFSPReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFSPReviewer)));
                        }
                        if (financeACT.WFBU1Approver != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFBU1Approver)));
                        }
                        if (financeACT.WFFinanceApprover1 != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFFinanceApprover1)));
                        }
                        if (financeACT.WFBU2Approver != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFBU2Approver)));
                        }
                    }
                    //else if (financeACT.WFCurrentApprover == "FinalApprover1")
                    //{
                    //    comment = financeACT.WFGlobalFinanceApprover1Comment;
                    //}
                    //else if (financeACT.WFCurrentApprover == "FinalApprover2")
                    //{
                    //    comment = financeACT.WFFinalApprover1Comment;
                    //}
                    //else if (financeACT.WFCurrentApprover == "FinalApprover3")
                    //{
                    //    comment = financeACT.WFFinalApprover2Comment;
                    //}
                    //else if (financeACT.WFCurrentApprover == "FinalApprover4")
                    //{
                    //    comment = financeACT.WFFinalApprover3Comment;
                    //}
                    else if (financeACT.WFCurrentApprover == "FinalApprover5")
                    {
                        comment = financeACT.WFFinalApprover5Comment;
                        //testemail = "reject-final5@gmail.com";
                        if (financeACT.WFEHSApprover != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSApprover)));
                        }
                        if (financeACT.WFEHSReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSReviewer)));
                        }
                        if (financeACT.WFSPReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFSPReviewer)));
                        }
                        if (financeACT.WFBU1Approver != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFBU1Approver)));
                        }
                        if (financeACT.WFFinanceApprover1 != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFFinanceApprover1)));
                        }
                        if (financeACT.WFBU2Approver != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFBU2Approver)));
                        }
                        if (financeACT.WFGlobalFinanceApprover1 != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFGlobalFinanceApprover1)));
                        }
                    }
                    else if (financeACT.WFCurrentApprover == "FinalApprover6" || financeACT.WFCurrentApprover == "Done")
                    {
                        comment = financeACT.WFFinalApprover6Comment;
                        //testemail = "reject-final6@gmail.com";
                        if (financeACT.WFEHSApprover != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSApprover)));
                        }
                        if (financeACT.WFEHSReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFEHSReviewer)));
                        }
                        if (financeACT.WFSPReviewer != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFSPReviewer)));
                        }
                        if (financeACT.WFBU1Approver != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFBU1Approver)));
                        }
                        if (financeACT.WFFinanceApprover1 != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFFinanceApprover1)));
                        }
                        if (financeACT.WFBU2Approver != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFBU2Approver)));
                        }
                        if (financeACT.WFGlobalFinanceApprover1 != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFGlobalFinanceApprover1)));
                        }
                        if (financeACT.WFFinalApprover5 != null)
                        {
                            oMessage.CC.Add(new MailAddress(getEmail(financeACT.WFFinalApprover5)));
                        }
                    }
                    //oMessage.CC.Add(new MailAddress(testemail));
                    emailAddress2 = financeACT.CreatedByEmail;
                    if (emailAddress2 != null)
                        oMessage.To.Add(new MailAddress(emailAddress2));

                    oMessage.CC.Add(new MailAddress("actapprovals@transitions.com"));
                    oMessage.Bcc.Add(new MailAddress(("coren.silva@essilor.com")));
                    oMessage.Bcc.Add(new MailAddress(("coren.silva@transitions.com")));

                    sb.AppendFormat("<p>The ACT submission <a href='{0}'>{1}</a> has been rejected by {2} with the following comments:<p>", configuration["ACTEditLink"].ToString() + financeACT.Id, financeACT.ProjectName, comment);
                    sb.Append("<p>You may fix any issues and resubmit. </ p>");

                    sMessage2 = sb.ToString();

                    SendMail(sMessage2, "Authorization for Capital Transaction(" + financeACT.WFCurrentApprover + ") - Rejected", oMessage);
                }

                else if (financeACT.Status == "Approved")
                {

                    string emailAddress3 = "", sMessage3 = "", sSubjectApproved = "";


                    MailMessage oMessageaapprovedALL = new MailMessage();
                    System.Text.StringBuilder sbapproved = new System.Text.StringBuilder();
                    oMessageaapprovedALL.To.Add(financeACT.CreatedByEmail);
                    oMessageaapprovedALL.Bcc.Add(new MailAddress(("coren.silva@essilor.com")));
                    oMessageaapprovedALL.Bcc.Add(new MailAddress(("coren.silva@transitions.com")));

                    sSubjectApproved = "Authorization for Capital Transaction Approved";

                    sbapproved.AppendFormat("<p>This is to notify that your ACT request, with the title: {0}, has passed all the approvals. {0}</p>", financeACT.ProjectName);
                    sbapproved.AppendFormat("<p>To view this request, click on this link: <a href='{0}'>{1}</a>.</ p>", configuration["ACTViewLink"].ToString() + financeACT.Id, configuration["ACTViewLink"].ToString() + financeACT.Id);

                    SendMail(sbapproved.ToString(), sSubjectApproved, oMessageaapprovedALL);


                    MailMessage oMessage = new MailMessage();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();

                    emailAddress3 = buresult.FinanceEmail;

                    string[] faApprover = emailAddress3.Split(";");
                    for (int i = 0; i < faApprover.Length; i++)
                    {
                        oMessage.To.Add(new MailAddress((faApprover[i].ToString().Trim())));
                    }

                    oMessage.CC.Add(new MailAddress("actapprovals@transitions.com"));
                    //oMessage.CC.Add(new MailAddress("approved-all@gmail.com"));
                    oMessage.CC.Add(new MailAddress(financeACT.CreatedByEmail));
                    oMessage.Bcc.Add(new MailAddress(("coren.silva@essilor.com")));
                    oMessage.Bcc.Add(new MailAddress(("coren.silva@transitions.com")));

                    //if (emailAddress3 != null)
                    //    oMessage.To.Add(new MailAddress(emailAddress3));

                    sb.Append("<p>Capital authorization request has been approved.</ p>");
                    sb.AppendFormat("<p>Kindly check the link below to view the request and assign a unique ACT#. <a href='{0}'>{1}</a><p>", configuration["ACTEditLink"].ToString() + financeACT.Id, financeACT.ProjectName);


                    sMessage3 = sb.ToString();

                    SendMail(sMessage3, "Authorization for Capital Transaction - ACT Approved assign ACT #", oMessage);
                }

            }
        }
        private void SendMail(String strMessage, String sSubject, MailMessage oMessageApprovers)
        {
            oMessageApprovers.From = new MailAddress("noreply@transitions.com", "noreply-");
            oMessageApprovers.Subject = sSubject;
            oMessageApprovers.Body = strMessage;
            oMessageApprovers.IsBodyHtml = true;
            SmtpClient oSMTPClient = new SmtpClient("smtp.gmail.com", 587);
            try
            {
                oSMTPClient.UseDefaultCredentials = false;
                oSMTPClient.EnableSsl = true;
                oSMTPClient.Credentials = new NetworkCredential("external.relay@transitions.com", "ydtrrputnrcimehf");
                // oSMTPClient.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);

                object userState = oMessageApprovers;
                oSMTPClient.SendCompleted += new SendCompletedEventHandler(SmtpClient_OnCompleted);

                oSMTPClient.SendAsync(oMessageApprovers, userState);

            }
            catch (Exception ex)
            {
            }

        }
        public static void SmtpClient_OnCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //Get the Original MailMessage object
            MailMessage mail = (MailMessage)e.UserState;

            //write out the subject
            string subject = mail.Subject;

            if (e.Cancelled)
            {
                Console.WriteLine("Send canceled for mail with subject [{0}].", subject);
            }
            if (e.Error != null)
            {
                Console.WriteLine("Error {1} occurred when sending mail [{0}] ", subject, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message [{0}] sent.", subject);
            }
        }

        string getEmail(string name)
        {
            var emailAddress = "";
            emailAddress = _contextEmployee.EmployeeMasters.Where(x => x.Name == name)
                     .Select(a => a.Email).FirstOrDefault();
            return emailAddress;
        }
    }
}