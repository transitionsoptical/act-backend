﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;

namespace InsightCore.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class FinanceACTBudgetCategoriesController : ControllerBase
    {
        private readonly ACTContext _context;

        public FinanceACTBudgetCategoriesController(ACTContext context)
        {
            _context = context;
        }

        // GET: api/FinanceACTBudgetCategories
        [HttpGet]
        public IEnumerable<FinanceACTBudgetCategory> GetFinanceACTBudgetCategorys()
        {
            return _context.FinanceACTBudgetCategorys;
        }

        // GET: api/FinanceACTBudgetCategories/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFinanceACTBudgetCategory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeACTBudgetCategory = await _context.FinanceACTBudgetCategorys.FindAsync(id);

            if (financeACTBudgetCategory == null)
            {
                return NotFound();
            }

            return Ok(financeACTBudgetCategory);
        }

        // PUT: api/FinanceACTBudgetCategories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFinanceACTBudgetCategory([FromRoute] int id, [FromBody] FinanceACTBudgetCategory financeACTBudgetCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != financeACTBudgetCategory.Id)
            {
                return BadRequest();
            }

            _context.Entry(financeACTBudgetCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FinanceACTBudgetCategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FinanceACTBudgetCategories
        [HttpPost]
        public async Task<IActionResult> PostFinanceACTBudgetCategory([FromBody] FinanceACTBudgetCategory financeACTBudgetCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.FinanceACTBudgetCategorys.Add(financeACTBudgetCategory);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFinanceACTBudgetCategory", new { id = financeACTBudgetCategory.Id }, financeACTBudgetCategory);
        }

        // DELETE: api/FinanceACTBudgetCategories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFinanceACTBudgetCategory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeACTBudgetCategory = await _context.FinanceACTBudgetCategorys.FindAsync(id);
            if (financeACTBudgetCategory == null)
            {
                return NotFound();
            }

            _context.FinanceACTBudgetCategorys.Remove(financeACTBudgetCategory);
            await _context.SaveChangesAsync();

            return Ok(financeACTBudgetCategory);
        }

        private bool FinanceACTBudgetCategoryExists(int id)
        {
            return _context.FinanceACTBudgetCategorys.Any(e => e.Id == id);
        }
    }
}