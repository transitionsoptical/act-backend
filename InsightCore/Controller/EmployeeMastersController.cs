﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;

namespace InsightCore.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeMastersController : ControllerBase
    {
        private readonly EmployeeMasterContext _context;

        public EmployeeMastersController(EmployeeMasterContext context)
        {
            _context = context;
        }

        // GET: api/EmployeeMasters
        [HttpGet]
        public IEnumerable<EmployeeMaster> GetEmployeeMasters()
        {
            return _context.EmployeeMasters;
        }

        // GET: api/EmployeeMasters/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmployeeMaster([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var employeeMaster = await _context.EmployeeMasters.FindAsync(id);

            if (employeeMaster == null)
            {
                return NotFound();
            }

            return Ok(employeeMaster);
        }

        // PUT: api/EmployeeMasters/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmployeeMaster([FromRoute] int id, [FromBody] EmployeeMaster employeeMaster)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != employeeMaster.Id)
            {
                return BadRequest();
            }

            _context.Entry(employeeMaster).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeMasterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EmployeeMasters
        [HttpPost]
        public async Task<IActionResult> PostEmployeeMaster([FromBody] EmployeeMaster employeeMaster)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.EmployeeMasters.Add(employeeMaster);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEmployeeMaster", new { id = employeeMaster.Id }, employeeMaster);
        }

        // DELETE: api/EmployeeMasters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployeeMaster([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var employeeMaster = await _context.EmployeeMasters.FindAsync(id);
            if (employeeMaster == null)
            {
                return NotFound();
            }

            _context.EmployeeMasters.Remove(employeeMaster);
            await _context.SaveChangesAsync();

            return Ok(employeeMaster);
        }

        private bool EmployeeMasterExists(int id)
        {
            return _context.EmployeeMasters.Any(e => e.Id == id);
        }
    }
}