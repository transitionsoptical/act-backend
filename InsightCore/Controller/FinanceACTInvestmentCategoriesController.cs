﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightCore.Model;

namespace InsightCore.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class FinanceACTInvestmentCategoriesController : ControllerBase
    {
        private readonly ACTContext _context;

        public FinanceACTInvestmentCategoriesController(ACTContext context)
        {
            _context = context;
        }

        // GET: api/FinanceACTInvestmentCategories
        [HttpGet]
        public IEnumerable<FinanceACTInvestmentCategory> GetFinanceACTInvestmentCategorys()
        {
            return _context.FinanceACTInvestmentCategorys;
        }

        // GET: api/FinanceACTInvestmentCategories/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFinanceACTInvestmentCategory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeACTInvestmentCategory = await _context.FinanceACTInvestmentCategorys.FindAsync(id);

            if (financeACTInvestmentCategory == null)
            {
                return NotFound();
            }

            return Ok(financeACTInvestmentCategory);
        }

        // PUT: api/FinanceACTInvestmentCategories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFinanceACTInvestmentCategory([FromRoute] int id, [FromBody] FinanceACTInvestmentCategory financeACTInvestmentCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != financeACTInvestmentCategory.Id)
            {
                return BadRequest();
            }

            _context.Entry(financeACTInvestmentCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FinanceACTInvestmentCategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FinanceACTInvestmentCategories
        [HttpPost]
        public async Task<IActionResult> PostFinanceACTInvestmentCategory([FromBody] FinanceACTInvestmentCategory financeACTInvestmentCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.FinanceACTInvestmentCategorys.Add(financeACTInvestmentCategory);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFinanceACTInvestmentCategory", new { id = financeACTInvestmentCategory.Id }, financeACTInvestmentCategory);
        }

        // DELETE: api/FinanceACTInvestmentCategories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFinanceACTInvestmentCategory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var financeACTInvestmentCategory = await _context.FinanceACTInvestmentCategorys.FindAsync(id);
            if (financeACTInvestmentCategory == null)
            {
                return NotFound();
            }

            _context.FinanceACTInvestmentCategorys.Remove(financeACTInvestmentCategory);
            await _context.SaveChangesAsync();

            return Ok(financeACTInvestmentCategory);
        }

        private bool FinanceACTInvestmentCategoryExists(int id)
        {
            return _context.FinanceACTInvestmentCategorys.Any(e => e.Id == id);
        }
    }
}