﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InsightCore.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FinanceACTBudgetCategorys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CategoryDescription = table.Column<string>(nullable: true),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinanceACTBudgetCategorys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FinanceACTBusinessUnits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true),
                    EHSApproverEmail = table.Column<string>(nullable: true),
                    EHSApproverName = table.Column<string>(nullable: true),
                    EHSReviewerEmail = table.Column<string>(nullable: true),
                    EHSReviewerName = table.Column<string>(nullable: true),
                    AdditionalApproverEmail = table.Column<string>(nullable: true),
                    AdditionalApproverName = table.Column<string>(nullable: true),
                    BusinessUnitApprover1Email = table.Column<string>(nullable: true),
                    BusinessUnitApprover1Name = table.Column<string>(nullable: true),
                    BusinessUnitApprover2Email = table.Column<string>(nullable: true),
                    BusinessUnitApprover2Name = table.Column<string>(nullable: true),
                    SPReviewerEmail = table.Column<string>(nullable: true),
                    SPReviewerName = table.Column<string>(nullable: true),
                    FinanceApprover1Email = table.Column<string>(nullable: true),
                    FinanceApprover1Name = table.Column<string>(nullable: true),
                    GlobalFinanceApprover1Email = table.Column<string>(nullable: true),
                    GlobalFinanceApprover1Name = table.Column<string>(nullable: true),
                    FinalApprover1Email = table.Column<string>(nullable: true),
                    FinalApprover1Name = table.Column<string>(nullable: true),
                    FinalApprover2Email = table.Column<string>(nullable: true),
                    FinalApprover2Name = table.Column<string>(nullable: true),
                    FinalApprover3Email = table.Column<string>(nullable: true),
                    FinalApprover3Name = table.Column<string>(nullable: true),
                    FinalApprover4Email = table.Column<string>(nullable: true),
                    FinalApprover4Name = table.Column<string>(nullable: true),
                    FinalApprover5Email = table.Column<string>(nullable: true),
                    FinalApprover5Name = table.Column<string>(nullable: true),
                    FinalApprover6Email = table.Column<string>(nullable: true),
                    FinalApprover6Name = table.Column<string>(nullable: true),
                    FinalApprover7Email = table.Column<string>(nullable: true),
                    FinalApprover7Name = table.Column<string>(nullable: true),
                    FinanceEmail = table.Column<string>(nullable: true),
                    FinanceName = table.Column<string>(nullable: true),
                    PurchasingEmail = table.Column<string>(nullable: true),
                    PurchasingName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinanceACTBusinessUnits", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FinanceACTClassifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true),
                    ApproverEmail = table.Column<string>(nullable: true),
                    ApproverName = table.Column<string>(nullable: true),
                    PriorApproverEmail = table.Column<string>(nullable: true),
                    PriorApproverName = table.Column<string>(nullable: true),
                    ClassificationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinanceACTClassifications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FinanceACTInvestmentCategorys",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InvestmentName = table.Column<string>(nullable: true),
                    InvestmentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinanceACTInvestmentCategorys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FinanceACTs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ACTNumber = table.Column<string>(nullable: true),
                    ACTAmount = table.Column<double>(nullable: false),
                    ACTAccountNumber = table.Column<string>(nullable: true),
                    ActRecordedtoMFGPRO = table.Column<bool>(nullable: false),
                    ApprovalInProgress = table.Column<bool>(nullable: false),
                    BudgetCategory = table.Column<string>(nullable: true),
                    Classification = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    DoesNeedEHSReview = table.Column<bool>(nullable: false),
                    EconomicLife = table.Column<double>(nullable: false),
                    EmailSentForACTNoAssigned = table.Column<bool>(nullable: false),
                    EmailSentForACTRecordedMFG = table.Column<bool>(nullable: false),
                    GCSCreated = table.Column<bool>(nullable: false),
                    InvesmentCategory = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false),
                    ProjectCashPaybackYears = table.Column<string>(nullable: true),
                    ProjectDescription = table.Column<string>(nullable: true),
                    ProjectIRR = table.Column<double>(nullable: false),
                    ProjectManagerEmail = table.Column<string>(nullable: true),
                    ProjectManagerName = table.Column<string>(nullable: true),
                    ProjectName = table.Column<string>(nullable: true),
                    ProjectROC = table.Column<double>(nullable: false),
                    ReRunWorkflowProcess = table.Column<bool>(nullable: false),
                    StartContruction = table.Column<DateTime>(nullable: false),
                    StartUpDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    WFApprover1Approval = table.Column<string>(nullable: true),
                    WFApprover1ApprovalDetails = table.Column<string>(nullable: true),
                    WFApprover1Comment = table.Column<string>(nullable: true),
                    WFApprover2Approval = table.Column<string>(nullable: true),
                    WFApprover2ApprovalDetails = table.Column<string>(nullable: true),
                    WFApprover2Comment = table.Column<string>(nullable: true),
                    WFApprover3Approval = table.Column<string>(nullable: true),
                    WFApprover3ApprovalDetails = table.Column<string>(nullable: true),
                    WFApprover3Comment = table.Column<string>(nullable: true),
                    WFApprover4Approval = table.Column<string>(nullable: true),
                    WFApprover4ApprovalDetails = table.Column<string>(nullable: true),
                    WFApprover4Comment = table.Column<string>(nullable: true),
                    WFApprover5Approval = table.Column<string>(nullable: true),
                    WFApprover5ApprovalDetails = table.Column<string>(nullable: true),
                    WFApprover5Comment = table.Column<string>(nullable: true),
                    WFApprover6Approval = table.Column<string>(nullable: true),
                    WFApprover6ApprovalDetails = table.Column<string>(nullable: true),
                    WFApprover6Comment = table.Column<string>(nullable: true),
                    WFApprover7Approval = table.Column<string>(nullable: true),
                    WFApprover7ApprovalDetails = table.Column<string>(nullable: true),
                    WFApprover7Comment = table.Column<string>(nullable: true),
                    WFBU1Approval = table.Column<string>(nullable: true),
                    WFBU1ApprovalDetails = table.Column<string>(nullable: true),
                    WFBU1Comment = table.Column<string>(nullable: true),
                    WFBU2Approval = table.Column<string>(nullable: true),
                    WFBU2ApprovalDetails = table.Column<string>(nullable: true),
                    WFBU2Comment = table.Column<string>(nullable: true),
                    WFBU3Approval = table.Column<string>(nullable: true),
                    WFBU3ApprovalDetails = table.Column<string>(nullable: true),
                    WFBU3Comment = table.Column<string>(nullable: true),
                    WFClassificationByDivisionApproval = table.Column<string>(nullable: true),
                    WFDivisionApprovalDetails = table.Column<string>(nullable: true),
                    WFClassificationbyDivisionComment = table.Column<string>(nullable: true),
                    WFCurrentApprover = table.Column<string>(nullable: true),
                    WFCurrentApproverEmail = table.Column<string>(nullable: true),
                    WFCurrentApproverName = table.Column<string>(nullable: true),
                    WFEHSApproval = table.Column<string>(nullable: true),
                    WFEHSApprovalDetails = table.Column<string>(nullable: true),
                    WFEHSApproverApproval = table.Column<string>(nullable: true),
                    WFEHSApproverApprovalDetails = table.Column<string>(nullable: true),
                    WFEHSApproverComment = table.Column<string>(nullable: true),
                    WFEHSComment = table.Column<string>(nullable: true),
                    WFEHSReviewer2 = table.Column<string>(nullable: true),
                    WFEHSReviewer2Details = table.Column<string>(nullable: true),
                    WFEHSReviewer2Comment = table.Column<string>(nullable: true),
                    WFFinance1Approval = table.Column<string>(nullable: true),
                    WFFinance1ApprovalDetails = table.Column<string>(nullable: true),
                    WFFinance1Comment = table.Column<string>(nullable: true),
                    WFFinance2Approval = table.Column<string>(nullable: true),
                    WFFinance2ApprovalDetails = table.Column<string>(nullable: true),
                    WFFinance2Comment = table.Column<string>(nullable: true),
                    WFPriorDivApproval = table.Column<string>(nullable: true),
                    WFPriorDivApprovalDetails = table.Column<string>(nullable: true),
                    WFPriorDivComment = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    CreatedByEmail = table.Column<string>(nullable: true),
                    WFEHSApprover = table.Column<string>(nullable: true),
                    WFEHSReviewer = table.Column<string>(nullable: true),
                    WFEHSReviewerApproval = table.Column<string>(nullable: true),
                    WFEHSReviewerComment = table.Column<string>(nullable: true),
                    WFSPReviewer = table.Column<string>(nullable: true),
                    WFSPReviewerApproval = table.Column<string>(nullable: true),
                    WFSPReviewerComment = table.Column<string>(nullable: true),
                    WFBU1Approver = table.Column<string>(nullable: true),
                    WFBU1ApproverApproval = table.Column<string>(nullable: true),
                    WFBU1ApproverComment = table.Column<string>(nullable: true),
                    WFBU2Approver = table.Column<string>(nullable: true),
                    WFBU2ApproverApproval = table.Column<string>(nullable: true),
                    WFBU2ApproverComment = table.Column<string>(nullable: true),
                    WFFinanceApprover1 = table.Column<string>(nullable: true),
                    WFFinanceApprover1Approval = table.Column<string>(nullable: true),
                    WFFinanceApprover1Comment = table.Column<string>(nullable: true),
                    WFGlobalFinanceApprover1 = table.Column<string>(nullable: true),
                    WFGlobalFinanceApprover1Approval = table.Column<string>(nullable: true),
                    WFGlobalFinanceApprover1Comment = table.Column<string>(nullable: true),
                    WFFinalApprover1 = table.Column<string>(nullable: true),
                    WFFinalApprover1Approval = table.Column<string>(nullable: true),
                    WFFinalApprover1Comment = table.Column<string>(nullable: true),
                    WFFinalApprover2 = table.Column<string>(nullable: true),
                    WFFinalApprover2Approval = table.Column<string>(nullable: true),
                    WFFinalApprover2Comment = table.Column<string>(nullable: true),
                    WFFinalApprover3 = table.Column<string>(nullable: true),
                    WFFinalApprover3Approval = table.Column<string>(nullable: true),
                    WFFinalApprover3Comment = table.Column<string>(nullable: true),
                    WFFinalApprover4 = table.Column<string>(nullable: true),
                    WFFinalApprover4Approval = table.Column<string>(nullable: true),
                    WFFinalApprover4Comment = table.Column<string>(nullable: true),
                    WFFinalApprover5 = table.Column<string>(nullable: true),
                    WFFinalApprover5Approval = table.Column<string>(nullable: true),
                    WFFinalApprover5Comment = table.Column<string>(nullable: true),
                    WFFinalApprover6 = table.Column<string>(nullable: true),
                    WFFinalApprover6Approval = table.Column<string>(nullable: true),
                    WFFinalApprover6Comment = table.Column<string>(nullable: true),
                    WFFinalApprover7 = table.Column<string>(nullable: true),
                    WFFinalApprover7Approval = table.Column<string>(nullable: true),
                    WFFinalApprover7Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinanceACTs", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FinanceACTBudgetCategorys");

            migrationBuilder.DropTable(
                name: "FinanceACTBusinessUnits");

            migrationBuilder.DropTable(
                name: "FinanceACTClassifications");

            migrationBuilder.DropTable(
                name: "FinanceACTInvestmentCategorys");

            migrationBuilder.DropTable(
                name: "FinanceACTs");
        }
    }
}
