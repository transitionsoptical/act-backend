﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
    public class FinanceActLocation
    {
        [Key]
        public int id { get; set; }
        public string title { get; set; }
        public string acttype{ get; set; }

    }
}
