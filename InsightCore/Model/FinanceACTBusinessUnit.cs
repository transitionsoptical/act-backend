﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
	public class FinanceACTBusinessUnit
	{
		[Key]
		public int Id { get; set; }

		public string Title { get; set; }
		public string EHSApproverEmail { get; set; }
		public string EHSApproverName { get; set; }
		public string EHSReviewerEmail { get; set; }
		public string EHSReviewerName { get; set; }
		public string AdditionalApproverEmail { get; set; }
		public string AdditionalApproverName { get; set; }
		public string BusinessUnitApprover1Email { get; set; }
		public string BusinessUnitApprover1Name { get; set; }
		public string BusinessUnitApprover2Email { get; set; }
		public string BusinessUnitApprover2Name { get; set; }
		public string SPReviewerEmail { get; set; }
		public string SPReviewerName { get; set; }
        public string FinanceApprover1Email { get; set; }
        public string FinanceApprover1Name { get; set; }
        public string GlobalFinanceApprover1Email { get; set; }
        public string GlobalFinanceApprover1Name { get; set; }
        public string FinalApprover1Email { get; set; }
		public string FinalApprover1Name { get; set; }
		public string FinalApprover2Email { get; set; }
		public string FinalApprover2Name { get; set; }
		public string FinalApprover3Email { get; set; }
		public string FinalApprover3Name { get; set; }
		public string FinalApprover4Email { get; set; }
		public string FinalApprover4Name { get; set; }
		public string FinalApprover5Email { get; set; }
		public string FinalApprover5Name { get; set; }
		public string FinalApprover6Email { get; set; }
		public string FinalApprover6Name { get; set; }
		public string FinalApprover7Email { get; set; }
		public string FinalApprover7Name { get; set; }
		public string FinanceEmail { get; set; }
		public string FinanceName { get; set; }
		public string PurchasingEmail { get; set; }
		public string PurchasingName { get; set; }
		
	}
}
