﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
	public class FinanceACT
	{
		[Key]
		public int Id { get; set; }

		public string ACTNumber { get; set; }
		public Double ACTAmount { get; set; }
		public string ACTAccountNumber { get; set; }
		public bool ActRecordedtoMFGPRO { get; set; }
		public bool ApprovalInProgress { get; set; }
		public string BudgetCategory { get; set; }
		public string Classification { get; set; }
		public DateTime Created { get; set; }
		public bool DoesNeedEHSReview { get; set; }
		public double EconomicLife { get; set; }
		public bool EmailSentForACTNoAssigned { get; set; }
		public bool EmailSentForACTRecordedMFG { get; set; }
		public bool GCSCreated { get; set; }
		public string InvesmentCategory { get; set; }
		public string Location { get; set; }
        public string Region { get; set; }
        public string Entity { get; set; }
        public DateTime Modified { get; set; }
		public string ProjectCashPaybackYears { get; set; }
		public string ProjectDescription { get; set; }
		public double ProjectIRR { get; set; }
		public string ProjectManagerEmail { get; set; }
		public string ProjectManagerName { get; set; }
		public string ProjectName { get; set; }
		public double ProjectROC { get; set; }
		public bool ReRunWorkflowProcess { get; set; }
		public DateTime StartContruction { get; set; }
		public DateTime StartUpDate { get; set; }
		public string Status { get; set; }
		public string WFApprover1Approval { get; set; }
		public string WFApprover1ApprovalDetails { get; set; }
		public string WFApprover1Comment { get; set; }
		public string WFApprover2Approval { get; set; }
		public string WFApprover2ApprovalDetails { get; set; }
		public string WFApprover2Comment { get; set; }
		public string WFApprover3Approval { get; set; }
		public string WFApprover3ApprovalDetails { get; set; }
		public string WFApprover3Comment { get; set; }
		public string WFApprover4Approval { get; set; }
		public string WFApprover4ApprovalDetails { get; set; }
		public string WFApprover4Comment { get; set; }
		public string WFApprover5Approval { get; set; }
		public string WFApprover5ApprovalDetails { get; set; }
		public string WFApprover5Comment { get; set; }
		public string WFApprover6Approval { get; set; }
		public string WFApprover6ApprovalDetails { get; set; }
		public string WFApprover6Comment { get; set; }
		public string WFApprover7Approval { get; set; }
		public string WFApprover7ApprovalDetails { get; set; }
		public string WFApprover7Comment { get; set; }
		public string WFBU1Approval { get; set; }
		public string WFBU1ApprovalDetails { get; set; }
		public string WFBU1Comment { get; set; }
		public string WFBU2Approval { get; set; }
		public string WFBU2ApprovalDetails { get; set; }
		public string WFBU2Comment { get; set; }
		public string WFBU3Approval { get; set; }
		public string WFBU3ApprovalDetails { get; set; }
		public string WFBU3Comment { get; set; }
		public string WFClassificationByDivisionApproval { get; set; }
		public string WFDivisionApprovalDetails { get; set; }
		public string WFClassificationbyDivisionComment { get; set; }
		public string WFCurrentApprover { get; set; }
		public string WFCurrentApproverEmail { get; set; }
		public string WFCurrentApproverName { get; set; }
		public string WFEHSApproval { get; set; }
		public string WFEHSApprovalDetails { get; set; }
		public string WFEHSApproverApproval { get; set; }
		public string WFEHSApproverApprovalDetails { get; set; }
		public string WFEHSApproverComment { get; set; }
		public string WFEHSComment { get; set; }
		public string WFEHSReviewer2 { get; set; }
		public string WFEHSReviewer2Details { get; set; }
		public string WFEHSReviewer2Comment { get; set; }
		public string WFFinance1Approval { get; set; }
		public string WFFinance1ApprovalDetails { get; set; }
		public string WFFinance1Comment { get; set; }
		public string WFFinance2Approval { get; set; }
		public string WFFinance2ApprovalDetails { get; set; }
		public string WFFinance2Comment { get; set; }
		public string WFPriorDivApproval { get; set; }
		public string WFPriorDivApprovalDetails { get; set; }
		public string WFPriorDivComment { get; set; }
		public string CreatedBy { get; set; }
		public string ModifiedBy { get; set; }
        public string CreatedByEmail { get; set; }
        public string WFEHSApprover { get; set; }
        public string WFEHSReviewer { get; set; }
        public string WFEHSReviewerApproval { get; set; }
        public string WFEHSReviewerComment { get; set; }
        public string WFSPReviewer { get; set; }
        public string WFSPReviewerApproval { get; set; }
        public string WFSPReviewerComment { get; set; }
        public string WFBU1Approver { get; set; }
        public string WFBU1ApproverApproval { get; set; }
        public string WFBU1ApproverComment { get; set; }
        public string WFBU2Approver { get; set; }
        public string WFBU2ApproverApproval { get; set; }
        public string WFBU2ApproverComment { get; set; }
        public string WFFinanceApprover1 { get; set; }
        public string WFFinanceApprover1Approval { get; set; }
        public string WFFinanceApprover1Comment { get; set; }
        public string WFGlobalFinanceApprover1 { get; set; }
        public string WFGlobalFinanceApprover1Approval { get; set; }
        public string WFGlobalFinanceApprover1Comment { get; set; }
        public string WFFinalApprover1 { get; set; }
        public string WFFinalApprover1Approval { get; set; }
        public string WFFinalApprover1Comment { get; set; }
        public string WFFinalApprover2 { get; set; }
        public string WFFinalApprover2Approval { get; set; }
        public string WFFinalApprover2Comment { get; set; }
        public string WFFinalApprover3 { get; set; }
        public string WFFinalApprover3Approval { get; set; }
        public string WFFinalApprover3Comment { get; set; }
        public string WFFinalApprover4 { get; set; }
        public string WFFinalApprover4Approval { get; set; }
        public string WFFinalApprover4Comment { get; set; }
        public string WFFinalApprover5 { get; set; }
        public string WFFinalApprover5Approval { get; set; }
        public string WFFinalApprover5Comment { get; set; }
        public string WFFinalApprover6 { get; set; }
        public string WFFinalApprover6Approval { get; set; }
        public string WFFinalApprover6Comment { get; set; }
        public string WFFinalApprover7 { get; set; }
        public string WFFinalApprover7Approval { get; set; }
        public string WFFinalApprover7Comment { get; set; }
        public string LastApprovedDate { get; set; }
    }
}
