﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
    public class EmployeeMasterContext : DbContext
    {
        public EmployeeMasterContext(DbContextOptions<EmployeeMasterContext> options) : base(options)
        {

        }

        public DbSet<EmployeeMaster> EmployeeMasters { get; set; }
    }
}
