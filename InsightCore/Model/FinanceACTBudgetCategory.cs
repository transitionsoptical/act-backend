﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
	public class FinanceACTBudgetCategory
	{
		[Key]
		public int Id { get; set; }

		public string CategoryDescription { get; set; }
		public int CategoryId { get; set; }
	}
}
