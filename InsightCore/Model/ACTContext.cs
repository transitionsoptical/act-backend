﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
	public class ACTContext : DbContext
	{
		public ACTContext(DbContextOptions<ACTContext> options) : base(options)
		{

		}

		public DbSet<FinanceACT> FinanceACTs { get; set; }

		public DbSet<FinanceACTBudgetCategory> FinanceACTBudgetCategorys { get; set; }

		public DbSet<FinanceACTClassification> FinanceACTClassifications { get; set; }

		public DbSet<FinanceACTInvestmentCategory> FinanceACTInvestmentCategorys { get; set; }

		public DbSet<FinanceACTBusinessUnit> FinanceACTBusinessUnits { get; set; }
        public DbSet<FinanceActLocation> FinanceActLocation { get; set; }
    }
}
