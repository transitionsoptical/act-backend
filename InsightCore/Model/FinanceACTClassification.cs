﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
	public class FinanceACTClassification
	{
		[Key]
		public int Id { get; set; }

		public string Title { get; set; }
		public string ApproverEmail { get; set; }
		public string ApproverName { get; set; }
		public string PriorApproverEmail { get; set; }
		public string PriorApproverName { get; set; }
		public int ClassificationId { get; set; }
	}
}
