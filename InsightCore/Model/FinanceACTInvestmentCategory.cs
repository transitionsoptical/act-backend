﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightCore.Model
{
	public class FinanceACTInvestmentCategory
	{
		[Key]
		public int Id { get; set; }

		public string InvestmentName { get; set; }
		public int InvestmentId { get; set; }
	}
}
